//
//  GradientView.swift
//  Shotglas
//
//  Created by XavierTanXY on 14/1/18.
//  Copyright © 2018 Shotglas. All rights reserved.
//

import Foundation
import UIKit

class GradientView: UIView {
    
    // Default Colors
    var colors:[UIColor] = [UIColor.red, UIColor.blue]
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        // Must be set when the rect is drawn
//        self.bounds.size.height = 180
        setGradient(color1: colors[0], color2: colors[1])
    }
//    override func drawRect(rect: CGRect) {
//        super.draw(rect)
//        // Must be set when the rect is drawn
//        setGradient(colors[0], color2: colors[1])
//    }
    
    func setGradient(color1: UIColor, color2: UIColor) {
        
        let colorTop =  UIColor(red: 255.0/255.0, green: 149.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 255.0/255.0, green: 94.0/255.0, blue: 58.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame.size.width = self.bounds.width
        gradientLayer.frame.size.height = 667
        
        
        //        self.view.layer.insertSublayer(gradientLayer)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    override func layoutSubviews() {
        
        // Ensure view has a transparent background color (not required)
        backgroundColor = UIColor.clear
        
    }
    
}
