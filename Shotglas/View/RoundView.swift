//
//  RoundView.swift
//  Shotglas
//
//  Created by XavierTanXY on 11/1/18.
//  Copyright © 2018 Shotglas. All rights reserved.
//

import Foundation
import UIKit

class RoundView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //layer.shadowColor = UIColor(red: SHADOW_GRAY, green: SHADOW_GRAY, blue: SHADOW_GRAY, alpha: 0.6).cgColor
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 10.0
        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        layer.cornerRadius = 15.0
        
    }
}

