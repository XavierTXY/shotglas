//
//  HomeCell.swift
//  Shotglas
//
//  Created by XavierTanXY on 9/1/18.
//  Copyright © 2018 Shotglas. All rights reserved.
//

import UIKit
import TagListView
import Cosmos

class HomeCell: UITableViewCell {

    @IBOutlet weak var bgImg: UIImageView!
    @IBOutlet weak var catImg: UIImageView!
    
    @IBOutlet weak var openView: TagListView!
    @IBOutlet weak var distanceView: TagListView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var starsView: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(place: Place) {
        
        distanceView.alignment = .right
        distanceView.tagBackgroundColor = UIColor(displayP3Red: 33.0/255.0, green: 150.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        if( place.distance >= 1000 ) {
            var distanceInKM = Double(place.distance)/1000.0
            distanceView.addTag("\(round(distanceInKM))km")
        } else {
            distanceView.addTag("\(place.distance)m")
        }
        

        if( place.open == "true" ) {
            openView.addTag("OPEN")
            openView.tagBackgroundColor = UIColor(displayP3Red: 76.0/255.0, green: 175.0/255.0, blue: 80.0/255.0, alpha: 1.0)
        } else if( place.open == "false" ) {
            openView.addTag("CLOSED")
            openView.tagBackgroundColor = UIColor(displayP3Red: 244.0/255.0, green: 67.0/255.0, blue: 54.0/255.0, alpha: 1.0)
        } else {
            openView.addTag("UNKNOWN")
            openView.tagBackgroundColor = UIColor.gray
//                UIColor(displayP3Red: 244.0/255.0, green: 67.0/255.0, blue: 54.0/255.0, alpha: 1.0)
        }
        
        nameLbl.text = place.name
        ratingLbl.text = "\(place.rating)"
        starsView.rating = place.rating
        starsView.settings.fillMode = .precise
        
        
    }
    

}
