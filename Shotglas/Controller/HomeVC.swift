//
//  HomeVC.swift
//  Shotglas
//
//  Created by XavierTanXY on 9/1/18.
//  Copyright © 2018 Shotglas. All rights reserved.
//

import UIKit
import CoreLocation
import CoreLocation
import SDWebImage

@IBDesignable
class HomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {

    @IBOutlet weak var tableView: UITableView!
    var places = [Place]()
    var currentLatitude: String!
    var currentLongitude: String!
    
    var  locationManager = CLLocationManager()
    var didFindLocation: Bool!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    
//        self.tabBarController?.tabBar.shadowImage = UIImage()
//        self.tabBarController?.tabBar.setValue(true, forKey: "_hidesShadow")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        places = [Place]()

        self.locationManager.startUpdatingLocation()
        didFindLocation = false

    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        var location = locations[0]
        var latitude = location.coordinate.latitude
        var longitude = location.coordinate.longitude
        
        self.currentLatitude = ("\(latitude)")
        self.currentLongitude =  ("\(longitude)")
        
        print(self.currentLatitude)
        print(self.currentLongitude)
        
        
        
        if( didFindLocation == false ) {
            fetchData()
            didFindLocation = true
            self.locationManager.stopUpdatingLocation()
        }

        
        
//        if( self.latitude != "" && self.longitude != "" ) {
//            completionHandler(true)
//        }
        
    }
    
    
    
    func fetchData() {


        var url : NSString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(currentLatitude!),\(currentLongitude!)&radius=10000&rankBy=distance&types=night_club|bar&keywords=bar|night%20club|lounge|disco|sports%20bar|karaoke&key=\(API_KEY)" as NSString
        var urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        var searchURL : URL = URL(string: urlStr as String)!
        print(searchURL)
        

        let task = URLSession.shared.dataTask(with: searchURL) {(data, response, error) in

            guard error == nil else {
                print("returning error")
                return
            }

            guard let content = data else {
                print("not returning data")
                return
            }

            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                print("Error")
                return
            }


            if let jsonObj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary {
//                 print(jsonObj!.value(forKey: "results")!)
                
                //getting the avengers tag array from json and converting it to NSArray
                if let placesArray = jsonObj!.value(forKey: "results") as? NSArray {
                    //looping through all the elements
                    for place in placesArray {
                        
                        var newPlace = Place()
                        
                        //converting the element to a dictionary
                        if let placeDict = place as? NSDictionary {
                            

                            if let geo = placeDict.value(forKey: "geometry") as? [String: Any]{
                                
                                if let location = geo["location"] as? [String: Any] {
                                 
                                    let latitude = location["lat"] as! Double
                                    let longitude = location["lng"] as! Double

                                    if let la = Double(self.currentLatitude) {
                                        if let lng = Double(self.currentLongitude) {
                                            let distance = self.calcDistance(lat: la, lng: lng, lat2: latitude, lng2: longitude)
                                            newPlace.distance = Int(distance)
//                                            print("distance")
//                                            print(distance)
                                        }
                                        
                                    }

                                }
                            }
                            
                            if let icon = placeDict.value(forKey: "icon") as? String{
//                                print(icon)
                                
                                newPlace.type = icon
                            }
                            
                            if let name = placeDict.value(forKey: "name") as? String {
//                                print(name)
                                newPlace.name = name
                            }
                            
                            //This can be unknown
                            if let openHour = placeDict.value(forKey: "opening_hours") as? [String: Any] {
                                let open = openHour["open_now"] as! Bool
                    
                                if( open == true ) {
                                    newPlace.open = "true"
                                } else if( open == false ) {
                                    newPlace.open = "false"
                                } else {
                                    //Unknow
                                    newPlace.open = "unknown"
                                }
                                
                                print(open)
                            } else {
                                newPlace.open = "unknown"
                            }
                            
                            //This can be unknown
                            if let rating = placeDict.value(forKey: "rating") as? Double {
                                newPlace.rating = rating
//                                print(rating)
                            } else {
                                newPlace.rating = 0.0
                            }
                            
                            if let place_id = placeDict.value(forKey: "place_id") as? String {
//                                print(place_id)
                                newPlace.id = place_id
                            }
                            
                            
                            if let tags = placeDict.value(forKey: "types") as? [String] {
                                newPlace.tags = tags
//                                print(tags)
                            }
                            
                            //This is incorrect for now
                            if let photoArray = placeDict.value(forKey: "photos") as? NSArray {
                                
                                
                                if let p = photoArray[0] as? NSDictionary {
                                    let photoRef = p["photo_reference"] as! String
                                    newPlace.imgUrl = photoRef
                                    print(photoRef)
                                }
                                
                            }
                            
                            
                            
                            self.places.append(newPlace)
                            
                        }
                    }
                }
            }


            DispatchQueue.main.async {
                self.tableView.reloadData()
            }

        }

        task.resume()
    }
    
    func calcDistance(lat: Double, lng: Double, lat2: Double, lng2: Double) -> Double{
        
        
        
        let coordinate = CLLocation(latitude: lat, longitude: lng)
        let coordinate1 = CLLocation(latitude: lat2, longitude: lng2)
        
        let distanceInMeters = coordinate.distance(from: coordinate1) // result is in meters
        
        return distanceInMeters
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let p = places[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell") as? HomeCell
        
        cell?.selectionStyle = .none
        cell?.openView.removeAllTags()
        cell?.distanceView.removeAllTags()
        cell?.bgImg.sd_setImage(with: URL(string: "https://maps.googleapis.com/maps/api/place/photo?photo_reference=\(p.imgUrl)&maxwidth=720&key=\(API_KEY)"), placeholderImage: UIImage(named: ""))
        cell?.configureCell(place: p)
        
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "DetailVC", sender: nil)
//        self.tableView.deselectRow(at: indexPath, animated: true)
    }


}

