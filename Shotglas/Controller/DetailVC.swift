//
//  DetailVC.swift
//  Shotglas
//
//  Created by XavierTanXY on 14/1/18.
//  Copyright © 2018 Shotglas. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView


class DetailVC: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    var stretchyHeader: GSKStretchyHeaderView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let nibViews = Bundle.main.loadNibNamed("Header", owner: self, options: nil)
        //        UINib(nibName: "EmptyConfessionView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        self.stretchyHeader = nibViews!.first as! GSKStretchyHeaderView
        stretchyHeader.expansionMode = .topOnly
        
        // You can change the minimum and maximum content heights
        stretchyHeader.minimumContentHeight = 20 // you can replace the navigation bar with a stretchy header view
        stretchyHeader.maximumContentHeight = 180
//        stretchyHeader.contentShrinks = false
        
        setGradientBackground()
        
        self.scrollView.addSubview(self.stretchyHeader)
        
    }
    
    func setGradientBackground() {
        
        let colorTop =  UIColor(red: 255.0/255.0, green: 149.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 255.0/255.0, green: 94.0/255.0, blue: 58.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        
//        self.view.layer.insertSublayer(gradientLayer)
        self.stretchyHeader.layer.insertSublayer(gradientLayer, at: 0)
    }



}
