//
//  Place.swift
//  Shotglas
//
//  Created by XavierTanXY on 9/1/18.
//  Copyright © 2018 Shotglas. All rights reserved.
//

import Foundation

class Place {
    
    private var _imgUrl: String!
    private var _name: String!
    private var _rating: Double!
    private var _open: String!
    private var _distance: Int!
    private var _type: String!
    private var _latitude: Double!
    private var _longitude: Double!
    private var _id: String!
    private var _tags: [String]!
    
    var tags: [String] {
        get {
            return _tags
        }
        
        set(inTags) {
            self._tags = inTags
        }
    }
    
    var id: String {
        get {
            return _id
        }
        
        set(inID) {
            self._id = inID
        }
    }
    
    var imgUrl: String {
        get {
            return _imgUrl
        }
        
        set(inImgUrl) {
            self._imgUrl = inImgUrl
        }
    }
    
    var name: String {
        get {
            return _name
        }
        
        set(inName) {
            self._name = inName
        }
    }
    
    var rating: Double {
        
        get {
            return _rating
        }
        
        set(inRating) {
            self._rating = inRating;
        }
        
    }
    
    var open: String {
        
        get {
            return _open
        }
        
        set(inOpen) {
            self._open = inOpen
        }
    }
    
    var distance: Int {
        
        get {
           return _distance
        }
        
        set(inDistance) {
            self._distance = inDistance
        }
    }
    
    var type: String {
        
        get {
            return _type
        }
        
        set(inType) {
            self._type = inType
        }
    }
    
    var latitude: Double {
        
        get {
            return _latitude
        }
        
        set(inLatitude) {
            self._latitude = inLatitude
        }
    }
    var longitude: Double {
        
        get {
            return _longitude
        }
        
        set(inLongitude) {
            self._longitude = inLongitude
        }
    }
    
    init() {
        
    }
    
    init(name: String, imgUrl: String, rating: Double, distance: Int, type: String, open: String) {
        self.name = name
        self.imgUrl = imgUrl
        self.rating = rating
        self.distance = distance
        self.type = type
        self.open = open
        
    }

}
